package emanuelmarques.flashtorch;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity {
    ImageButton toggleBtn;
    Camera camera;
    Camera.Parameters parameters;
    boolean hasFlash;
    boolean isToggled;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        flashLight();
    }

    private void flashLight() {
        hasFlash = getApplicationContext()
                .getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (hasFlash) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this,
                        "Please provide Camera permission on your phone Settings",
                        Toast.LENGTH_LONG).show();

                hasFlash = false;
            }
        }

        MobileAds.initialize(MainActivity.this, "ca-app-pub-3882821033143506/4745230277");
        MainActivity.this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        setContentView(R.layout.activity_main);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (hasFlash) {
            if (camera != null) {
                camera.release();
                camera = null;
            }
            camera = Camera.open();
            parameters = camera.getParameters();
        }

        toggleBtn = (ImageButton) findViewById(R.id.toggleBtn);
        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isToggled) {
                    if (hasFlash) {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        camera.setParameters(parameters);
                        camera.startPreview();
                    }

                    toggleBtn.setImageResource(R.drawable.btn_switch_on);
                    toggleBtn.setBackgroundColor(Color.WHITE);
                    MainActivity.this.getWindow().getDecorView().setBackgroundColor(Color.WHITE);

                    isToggled = true;
                } else {
                    if (hasFlash) {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameters);
                        camera.stopPreview();
                    }
                    toggleBtn.setImageResource(R.drawable.btn_switch_off);
                    toggleBtn.setBackgroundColor(Color.BLACK);
                    MainActivity.this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

                    isToggled = false;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        flashLight();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (hasFlash && camera != null) {
            camera.release();
            camera = null;
        }
    }
}